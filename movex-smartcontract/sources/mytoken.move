
// SPDX-License-Identifier: MIT
module movex::mycoin {
    use sui::coin::{Self, Coin};
    use sui::balance::{Self, Supply, Balance};
    use sui::object::{Self, UID};
    use sui::transfer;
    use sui::tx_context::TxContext;
    use std::vector;

    /// The type identifier of coin. The coin will have a type
    /// tag of kind: `Coin<package_object::mycoin::MYCOIN>`
    /// Make sure that the name of the type matches the module's name.
    struct MYCOIN has drop {}

    /// Shared object holding mycoin assets and the capability.
    struct Reserve has key {
        id: UID,
        /// capability allowing the reserve to mint <MYCOIN>
        total_supply: Supply<MYCOIN>,
    }

    // Module initializer is called once on module publish.
    fun init(witness: MYCOIN, ctx: &mut TxContext) {
        // Get a cap for the coin put it in the reserve
        let total_supply = balance::create_supply<MYCOIN>(witness);

        transfer::share_object(Reserve {
            id: object::new(ctx),
            total_supply,
        })
    }

    /// === Writes ===

    // Mint MYCOIN coins
    // A vector of MYCOIN object IDs can be sent during mint, the balance will be consolidated,
    // MYCOIN objects will be destroyed and a new MYCOIN will all the balance will be minted.
    // In the initial mint, send and empty coins vector (send '[]').
    // NOTE: provided that is safe, the ideal situation would be get the vector of MYCOIN objects
    //      on-chain so we would not need to get the MYCOIN objects from the frontend
    // NOTE 2: This smart contract has not been audited and optimized for security. Use at your own risk!
    public entry fun mint(
        reserve: &mut Reserve, amount: u64, recipient: address, coins: vector<Coin<MYCOIN>>, ctx: &mut TxContext
    ) {
        // add up all coin object balances and destroy the coins
        let coin_balance_value=0;
        while  (!vector::is_empty(&coins)) {
            // pop the last MYCOIN element of the vector
            let coin = vector::pop_back(&mut coins);
            // get coins balance and destroy the coin object
            let former_balance = balance_destroy_coin(coin);
            // decrease supply and destroy balance object
            coin_balance_value = coin_balance_value + decrease_supply_return_balance_value(reserve, former_balance);
        };
        // Destroy vector as it is empty
        vector::destroy_empty(coins);
        // Increase supply by new amount and create a new balance of the coin with this value.
        let new_minted_balance = increase_supply_return_balance(reserve, coin_balance_value + amount);
        // check new minted balance is equal to total coins balances
        assert!(balance::value(&new_minted_balance) == amount + coin_balance_value, 0);
        // wrap balance into a coin to make it transferable
        let wrapped_balance_to_coin = wrap_balance_to_coin(new_minted_balance, ctx);
        // transfer the coin to the provided recipient address
        transfer::transfer(wrapped_balance_to_coin, recipient)
    }

    /// transfer MYCOIN amount to the stated address
    public entry fun transfer(
        reserve: &mut Reserve, amount: u64, recipient: address, coins: vector<Coin<MYCOIN>>, ctx: &mut TxContext
    ) {
        // add up all coin object balances and destroy the coins
        let coin_balance_value=0;
        while  (!vector::is_empty(&coins)) {
            // pop the last MYCOIN element of the vector
            let coin = vector::pop_back(&mut coins);
            // get coins balance and destroy the coin object
            let former_balance = balance_destroy_coin(coin);
            // decrease supply and destroy balance object
            coin_balance_value = coin_balance_value + decrease_supply_return_balance_value(reserve, former_balance);
        };
        // Destroy vector as it is empty
        vector::destroy_empty(coins);
        // create two balances
        // a. create a non-transferable balance = coin_balance_value - amount
        let non_transferrable_balance = increase_supply_return_balance(reserve, coin_balance_value - amount);
        // b. create a transferable balance = amount
        let transferrable_balance = increase_supply_return_balance(reserve, amount);
        // // wrap non_transferrable_balance into a coin
        let non_transferrable_balance_to_coin = wrap_balance_to_coin(non_transferrable_balance, ctx);
        // // transfer coin to the sender of the current transaction
        coin::keep(non_transferrable_balance_to_coin, ctx);
        // wrap balance into a coin to make it transferable
        let transferrable_balance_to_coin = wrap_balance_to_coin(transferrable_balance, ctx);
        // transfer the amount to the recipient address
        transfer::transfer(transferrable_balance_to_coin, recipient)
    }

    // Public helper functions to organize better the code

    // get coins balance and destroy the coin object
    public fun balance_destroy_coin(
        coin: Coin<MYCOIN>
    ) : Balance<MYCOIN> {
        coin::into_balance(coin)
    }

    // decrease supply and destroy balance object
    public fun decrease_supply_return_balance_value(
        reserve: &mut Reserve, balance: Balance<MYCOIN>
    ) : u64 {
        balance::decrease_supply(&mut reserve.total_supply, balance)
    }

    // Increase supply by new amount and create a new balance of the coin with this value.
    public fun increase_supply_return_balance(
        reserve: &mut Reserve, amount: u64
    ) : Balance<MYCOIN> {
        balance::increase_supply(&mut reserve.total_supply, amount)
    }

    // wrap balance into a coin to make it transferable
    public fun wrap_balance_to_coin(
        former_balance: Balance<MYCOIN>, ctx: &mut TxContext
    ) :  Coin<MYCOIN> {
        coin::from_balance(former_balance, ctx)
    }


    #[test_only]
    public fun init_for_testing(ctx: &mut TxContext) {
        init(MYCOIN {}, ctx)
    }

    // NOTE: the code below was the first one to mint and transfer without paying attention to multiple 
    //      objects of MYCOIN

    // Mint MYCOIN coins
    // public entry fun first_mint(
    //     reserve: &mut Reserve, amount: u64, recipient: address, ctx: &mut TxContext
    // ) {

    //     // Increase supply by amount and create a new balance of the coin with this value.
    //     let minted_balance = balance::increase_supply(&mut reserve.total_supply, amount);

    //     // wrap balance into a coin to make it transferable
    //     transfer::transfer(coin::from_balance(minted_balance, ctx), recipient)
    // }

    // /// transfer MYCOIN amount to the stated address
    // public entry fun transfer(
    //     coin: &mut Coin<MYCOIN>, amount: u64, recipient: address, ctx: &mut TxContext
    // ) {
    //     coin::split_and_transfer(coin, amount, recipient, ctx)
    // }

}