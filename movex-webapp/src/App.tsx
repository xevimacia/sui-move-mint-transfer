import React, { useEffect, useState, useRef } from 'react';
import './App.css';
import logo from './movex-main-image.png';
import logoMovex from './movex-logo.png';
import { ConnectButton, getAllWallets, useWallet, WalletProvider } from '@suiet/wallet-kit'
import "@suiet/wallet-kit/style.css";
import "./suiet-wallet-kit-custom.css";
import { JsonRpcProvider } from '@mysten/sui.js';

function App() {
  const provider = new JsonRpcProvider('https://fullnode.devnet.sui.io:443');
  const {
    wallet,
    connected,
    connecting,
    getAccounts,
    executeMoveCall,
  } = useWallet();

  // Constants to be edited after smart contract deployment
  const packageObjectId = "0x72a88672228d6e924dfe6a1370d65b5750118aea"
  const moduleName = "mycoin"
  const supplyID = "0x05047abfd96f6880848d775851ef8b922c8803f1"
  const typeDescription = "coin::Coin<0x72a88672228d6e924dfe6a1370d65b5750118aea::mycoin::MYCOIN>"

  const isMounted = useRef(false);

  const [accounts, setAccounts] = useState<string[]>([])
  const [initialState, setInitialState] = useState(false)
  const [myCoinObject, setMyCoinObject] = useState<string[]>([])
  const [balance, setBalance] = useState(0)
  const [numberCoins, setNumberCoins] = useState("")
  const [sentAddress, setSentAddress] = useState("")

  let objectsDetails: string[] = [];
  let tempBalance: number = 0;
  let tempMyCoinObject: string[] = [];

  var _ = require('lodash');

  useEffect(() => {
    if (!connected && isMounted.current) return
    (async function () {
      let result = await getAccounts();
      setAccounts(result);
      setInitialState(true);
    })()
  }, [connected])

  useEffect(() => {
    if (isMounted.current && initialState && connected) {
      handleGetObjects()
    } else {
      isMounted.current = true;
    }
  }, [connected, initialState])

  useEffect(() => {
    if (connecting) {
      setInitialState(false);
    }
  }, [connecting])

  // Handlers

  async function handleMint() {
    try {
      const data = {
        packageObjectId: packageObjectId,
        module: moduleName,
        function: "mint",
        typeArguments: [],
        arguments: [
          supplyID,
          500,
          accounts[0],
          myCoinObject,
        ],
        gasBudget: 30000,
      }
      const resData = await executeMoveCall(data);
      console.log('mint success', resData)
      tempMyCoinObject = []
      await handleGetObjects()
      alert('mint succeeded (see response in the console)')
    } catch (e) {
      console.error('mint failed', e)
      alert('mint failed (see response in the console)')
    }
  }

  async function handleTransfer(event: React.FormEvent<HTMLFormElement>) {
    try {
      // Preventing the page from reloading
      event.preventDefault();
      console.log('numberCoins', numberCoins)
      console.log('sentAddress', sentAddress)
      await handleGetObjects()
      if (Number(numberCoins) <= balance) {
        const data = {
          packageObjectId: packageObjectId,
          module: moduleName,
          function: "transfer",
          typeArguments: [],
          arguments: [
            supplyID,
            numberCoins,
            sentAddress,
            myCoinObject,
          ],
          gasBudget: 30000,
        }
        const resData = await executeMoveCall(data);
        console.log('transfer success', resData)
        tempMyCoinObject = []
        await handleGetObjects()
        alert('transfer succeeded (see response in the console)')
      } else {
        alert('Not enough balance. Mint more coins!')
      }
    } catch (e) {
      console.error('transfer failed', e)
      alert('transfer failed (see response in the console)')
    }
  }

  async function handleGetObjects() {
    try {
      // initialize myCoinObject
      tempMyCoinObject = [];
      // get objects owned by logged in wallet address
      let resData = await provider.getObjectsOwnedByAddress(
        accounts[0]
      );
      console.log('getObjects resData', resData);
      // Filter by typeDescription to get the list of objects that belong to the package
      let objectOwned = await resData.filter((b) => {
        return b.type.includes(
          typeDescription
        );
      });
      console.log('getObjects success', objectOwned);
      // console.log('objectID', objectOwned?.objectId)
      if (typeof objectOwned != "undefined") {
        objectOwned.forEach(element => {
          tempMyCoinObject.push(element.objectId)
        });
      }
      console.log('myCoinObject', tempMyCoinObject);
      setMyCoinObject(tempMyCoinObject);
    } catch (e) {
      console.error('handleGetObjects failed', e)
      alert('handleGetObjects failed (see response in the console)')
    } finally {
      // call object details async function
      tempBalance = 0;
      await tempMyCoinObject.forEach(element => {
        handleGetObjectsDetails(element)
      })
      console.log('objectsDetails', objectsDetails);
    }
  }

  async function handleGetObjectsDetails(object: string) {
    try {
      // get details of filtered objects by typeDescription
      let objectDetailsRes = await provider.getObject(object)
      console.log('objectDetailsRes', objectDetailsRes);
      // get the balance of each object using Lodash for deeply nested JSON objects
      tempBalance += _.get(objectDetailsRes, 'details.data.fields.balance')
      console.log("tempBalance", tempBalance);
      setBalance(tempBalance);
    } catch (e) {
      console.error('handleGetObjectsDetails failed', e)
      alert('handleGetObjects failed (see response in the console)')
    }

  }

  return (
    <div className="App">
      <header className="header">
        <img src={logoMovex} className="logo-movex" alt="movex-logo" />
        <div className={'connect-button'}>
          <ConnectButton />
        </div>
      </header>
      <main className={'main'}>
        {!connected ? (
          <div>
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Connect wallet to start
            </p>
          </div>
        ) : (
          <div>
            <div>
              <p>current wallet: {wallet ? wallet.adapter.name : "null"}</p>
              <p>
                wallet status:{" "}
                {connecting
                  ? "connecting"
                  : connected
                    ? "connected"
                    : "disconnected"}
              </p>
              <p>wallet account: {JSON.stringify(accounts)}</p>
            </div>
            <div className="container">
              <div >
                <div className="side-by-side" style={{ marginBottom: "20px", alignContent: "center" }}>
                  <div style={{ display: 'flex', flexDirection: 'row', width: 'auto', height: "35px" }}>
                    <div>
                      <label className="label" style={{ alignContent: 'center' }}>Mycoin Balance:</label>
                    </div>
                    <div style={{ alignContent: 'end', width: '70px' }}>
                      <label className="label" style={{ textAlign: 'end'}}>{balance.toLocaleString()}</label>
                    </div>
                    <div>
                      <label className="label" style={{ alignContent: 'center' }}>tokens</label>
                    </div>
                  </div>
                  <div>
                    <button type="submit" className="btn" onClick={handleMint}>Mint 500 tokens</button>
                  </div>
                </div>
              </div>
              <form onSubmit={handleTransfer}>
                <div className="side-by-side" style={{ marginBottom: "60px" }}>
                  <div className="side-by-side">
                    <div>
                      <label className="label">Coins</label>
                      <input
                        pattern="^-?[0-9]\d*\.?\d*$"
                        value={numberCoins}
                        onChange={(e) => {
                          const re = /^[0-9\b]+$/;
                          if (e.target.value === '' || re.test(e.target.value)) {
                            setNumberCoins(e.target.value)
                          }
                        }}
                        type="tel"
                        placeholder="Number of Coins"
                        className="field"
                      />
                    </div>
                    <div>
                      <label className="label">Address</label>
                      <input
                        value={sentAddress}
                        onChange={(e) => setSentAddress(e.target.value)}
                        type="text"
                        placeholder="Wallet Address"
                        className="field"
                      />
                    </div>
                  </div>
                  <div>
                    <button type="submit" className="btn">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

        )}
        <a
          className="App-link"
          href="https://www.movex.exchange/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn more about the First Dex on SUI
        </a>
      </main>
    </div>
  );
}

export default function withApp() {
  return (
    <WalletProvider supportedWallets={getAllWallets()}>
      <App />
    </WalletProvider>
  )
};